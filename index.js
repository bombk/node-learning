console.log("add");

var a = 20;
let b = 30;
const c = 40;
const x = 20

console.warn(a * b * b);

if (x === 20) {
    console.log("match");
}

else {
    console.log("not matched");
}

for (i = 0; i <= 10; i++) {
    console.log(i);
}

const arr = [1, 2, 3, 4, 5, 6]
console.log(arr);
console.log(arr[4]);


///export from app.js

const app = require('./app');
console.log(app);
console.log(app.y);
console.log(app.x);
console.log(app.z);

// filter used in js filter array using loop

// arr.filter((item)=>{
//     console.log(item);
// })

let result = arr.filter((item) => {
    // return item===6
    return item > 3

})

//console is global module
console.log(result);

//non-gloabl module
const fs = require('fs');
console.log("-->", __dirname);
console.log("-->", __filename);
fs.writeFileSync("hello.txt", "bom by bom")