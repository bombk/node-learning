const fs=require('fs')
const path=require('path')

const dirPath=path.join(__dirname,'curd')
console.log(dirPath);

//write file
// fs.writeFileSync('apple.txt','this is a apple file')
// fs.writeFileSync(`${dirPath}/hello.txt`,"simple test file")


//read files
fs.readFile(`${dirPath}/hello.txt`,'utf-8',(err,item)=>{
    console.log(item);


})


//append files
fs.appendFile(`${dirPath}/hello.txt`,' \nfile update',(err)=>{
    if(!err) console.log("file updated");
})

fs.rename(`${dirPath}/hello.txt`,`${dirPath}/hello.txt`,(err)=>{
    if(!err) console.log("file is rename");
})


//remove files
// fs.unlinkSync(`${dirPath}/hello.txt`)
