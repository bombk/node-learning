const fs=require('fs')
const path=require('path')

const dirPath=path.join(__dirname)
console.log(dirPath);

// fs.writeFileSync('apple.txt','this is a apple file')

for (i=0;i<5;i++){
    fs.writeFileSync(`${dirPath}/hello${i}.txt`,"simple test file")

}

fs.readdir(dirPath,(err,files)=>{
    // console.log(files); 
    files.forEach((item)=>{
        console.log(item);
    })
   
})