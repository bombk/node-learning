const {MongoClient}=require('mongodb'); //new style of writing
// const mongoclient=require('mongodb').MongoClient;

const url='mongodb://localhost:27017';
const database='ecom'
const client=new MongoClient(url)

async function getData(){
    let result=await client.connect();
    let db=result.db(database);
    let collection =db.collection('products');
    let response =await collection.find({brand:'apple'},{_id:1}).toArray();
    console.log(response);
}
getData();