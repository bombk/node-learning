const {MongoClient}=require('mongodb'); //new style of writing
// const mongoclient=require('mongodb').MongoClient;

const url='mongodb://10.21.7.55:27017';
const database='genieacs'
const client=new MongoClient(url)

async function getData(){
    let result=await client.connect();
    let db=result.db(database);
    let collection =db.collection('devices');
    let response =await collection.find({"InternetGatewayDevice.DeviceInfo.SoftwareVersion._value":"3FE49362HJJJ84"},{"_id":1}).toArray();
    console.log(response);
}
getData();