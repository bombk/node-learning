
const dbConnect=require('./mongodb')

//console.log(dbConnect());   give promises

//one method of feth data
dbConnect().then((resp)=>{
    //console.log(resp.find().toArray());
    resp.find().toArray().then((data)=>{
        console.log(data);
});
});


//another method morden method
const main =async ()=>{
    console.log("main function");
    let data =await dbConnect();
    data=await data.find().toArray();
    console.log(data);
}
main()