const {MongoClient}=require('mongodb'); //new style of writing
// const mongoclient=require('mongodb').MongoClient;

const url='mongodb://localhost:27017';
const database='ecom'
const client=new MongoClient(url)

async function dbConnect(){
    let result=await client.connect();
    let db=result.db(database);
    return collection =db.collection('products');
    // let response =await collection.find({brand:'apple'},{_id:1}).toArray();
    // consolae.log(response);
}

module.exports=dbConnect;
