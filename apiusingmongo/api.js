const express=require('express')
const dbConnect=require('./mongodb')
const mongoDb=require('mongodb')

const app=express()

app.use(express.json());

// app.get('',(req,resp)=>{
//     resp.send({name:'bom'})

// })

app.get('/',async (req,resp)=>{
    let data=await dbConnect()
    data=await data.find().toArray();
    // console.log(data);
    resp.send(data)

})

app.post('/',async (req,resp)=>{
    //resp.send({name:'bom'})
   let data=await dbConnect();
   let result=await data.insertOne(req.body)
   console.log(result);
//    resp.send(req.body)
    if(result.acknowledged){
        resp.send({message:"data insert successfully"})
    }

})

app.put('/',async (req,resp)=>{
    // resp.send({name:"put method test"})

    let data=await dbConnect()
    let result=await data.updateMany({ name:req.body.name},{ $set:req.body})

    if(result.acknowledged){
        resp.send({message:"sucessfly put data"})
    }

})

app.delete('/:id',async (req,resp)=>{
    // resp.send({message:"sucess"})
    console.log(req.params.id)///pass from url
    const data=await dbConnect();
    const result=await data.deleteOne({_id:new mongoDb.ObjectId(req.params.id)})
    
    if(result.acknowledged){
        resp.send({message:"done"})

    }
 
})

app.listen(5000)