const { Router } = require('express');
const express =require('express')
const reqFilter=require('./middleware')
app=express()




app.get('',(req,resp)=>{
    resp.send("welcome to home page")

});

app.get('/user',reqFilter,(req,resp)=>{
    resp.send('welcome to use page')
})

app.get('/nomiddleware',(req,resp)=>{
    resp.send('this page not have middleware ')
})


//apply middleware in group
const route=Router()
route.use(reqFilter);

route.get('/about',(req,resp)=>{
    resp.send("welcome to group middleware about section")
})
route.get('/contact',(req,resp)=>{
    resp.send("welcome to group middleware about contact")
})

route.get('/hello',(req,resp)=>{
    resp.send("welcome to group middleware about hello page")
})

app.use('/',route)


app.listen(5000)