const { response } = require('express');
const express=require('express')

const app=express()
app.set('view engine','ejs')

app.get('',(_,res)=>{
    res.render('profile')
})
app.get('/profile',(_,res)=>{
    const user={
        name:'bom',
        email:'bom@gmail.com',
        city:'kathmandu',
        skills:['php','js','node','python','react']
    }
    res.render('profile',{user})
})

app.get('/login',(_,resp)=>{
   resp.render('login')
})


app.listen(5001)